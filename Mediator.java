public class Mediator {
    private Nappi nappi;
    private Tuuletin tuuletin;
    private Virta virta;

    public void paina() {
        if (tuuletin.paalla) {
            tuuletin.kaynnista();
        } else {
            tuuletin.sammutaa();
        }
    }

    public void kaynnista() {
        Virta.kaynnista();
    }

    public void stop() {
        Virta.sammutaa();
    }
}

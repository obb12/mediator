public class Tuuletin {
  private Mediator mediator;
  public  boolean paalla = false;

  public void kaynnista() {
      mediator.kaynnista();
      paalla = true;
  }

  public void sammutaa() {
      paalla = false;
      mediator.stop();
  }
}
